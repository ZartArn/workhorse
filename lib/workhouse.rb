require 'open-uri'
require 'nokogiri'

module Workhorse
	class << self
		def lenta_ru
			page = open('http://rgsbank.ru/about/press-center/news/rss.php')
			doc = Nokogiri::XML(page)
			# puts doc

			doc.xpath('//channel/item').first(5).map do |item|
				{
					title: item.xpath('.//title').first.content,
					date: Time.parse(item.xpath('.//pubDate').first.content)
				}
			end
		end
	end
end
