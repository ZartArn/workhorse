//
//  <%= "#{name}ViewModel.m" %>
//  <%= config[:bundle_name] %>
//
//  Created by <%= config[:user_name] %> <%= config[:user_email] %> on <%= Time.now.strftime('%d.%m.%y') %>.
//  Copyright © <%= Time.now.strftime('%Y') %> <%= config[:company_name] %>. All rights reserved.
//

#import "<%= "#{name}ViewModel.h" %>"
#import <ReactiveCocoa.h>

@interface <%= "#{name}ViewModel" %> ()

@end

@implementation <%= "#{name}ViewModel" %>

- (instancetype)init {
    if (self = [super init]) {
        [self configure];
    }
    return self;
}

- (void)configure {
    
}

@end
