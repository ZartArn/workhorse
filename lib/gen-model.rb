require ('yaml')

class GeneratorSimpleModel < Thor

	include Thor::Actions

	attr_accessor :name

	@objc_types = {
		'object'	=> 'NSObject',
		'string'	=> 'NSString',
		'integer' 	=> 'NSNumber',
		'boolean'	=> 'NSNumber',
		'number'	=> 'NSNumber',
		'array' 	=> 'NSArray',
		'dict' 		=> 'NSDictionary',
		'date' 		=> 'NSDate'
	}

	@objc_view_types = {
		'view' 			=> 'UIView',
		'control' 		=> 'UIControl',
		'label' 		=> 'UILabel',
		'text_field' 	=> 'UITextField',
		'input' 		=> 'UITextField',
		'text_view' 	=> 'UITextView',
		'button' 		=> 'UIButton',
		'switch' 		=> 'UISwitch',
		'table' 		=> 'UITableView',
		'collection'	=> 'UICollectionView',
		'scroll'		=> 'UIScrollView',
		'image'			=> 'UIImageView',
		'image_pure'	=> 'UIImage',
		'web'			=> 'UIWebView',
		'web_view'		=> 'UIWebView',
		'progress'		=> 'UIProgressView'
	}

	def self.source_root
		File.dirname(__FILE__)
	end

    def self.create_config
      file_path = File.expand_path('../../config.yml', __FILE__)
      @config ||= YAML.load_file(file_path)
    end

	desc 'model', 'generate simple model'
	def model(model_name, *attr)

	    opt = GeneratorSimpleModel.create_config
		say opt, :green

		name = model_name #.to_s.capitalize
		@name = name

		@attributes = []
		attr.each do |attribute|
			field_name, type = attribute.split(':')
			objc_type = GeneratorSimpleModel.get_objc_type(type)
			@attributes << {
				:type => objc_type,
				:name => field_name
			}		
		end
		say @attributes, :blue
		say @attributes.map { |item| item[:name]  }, :green

		template "../template/models/simple_model.h.erb", "output/M#{name}.h", opt
		template "../template/models/simple_model.m.erb", "output/M#{name}.m", opt		
	end

	desc 'cell', 'generate simple tableView cell'
	def cell(model_name, *attr)

	    opt = GeneratorSimpleModel.create_config
		say opt, :green

		name = model_name #.to_s.capitalize
		@name = name

		@attributes = []
		attr.each do |attribute|
			field_name, type = attribute.split(':')
			objc_type = GeneratorSimpleModel.objc_view_type(type)
			@attributes << {
				:type => objc_type,
				:name => field_name
			}		
		end
		say @attributes, :blue

		template "../template/table/cell_view.h.erb", "output/#{name}Cell.h", opt
		template "../template/table/cell_view.m.erb", "output/#{name}Cell.m", opt		
		template "../template/table/cell_view_model.h.erb", "output/#{name}CellViewModel.h", opt
		template "../template/table/cell_view_model.m.erb", "output/#{name}CellViewModel.m", opt		
	end

	no_tasks do
		def self.get_objc_type(type_name)
			@objc_types[type_name]
		end
		def self.objc_view_type(type_name)
			@objc_view_types[type_name]
		end
	end

end