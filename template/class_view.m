//
//  <%= "#{name}View.m" %>
//  <%= config[:bundle_name] %>
//
//  Created by <%= config[:user_name] %> <%= config[:user_email] %> on <%= Time.now.strftime('%d.%m.%y') %>.
//  Copyright © <%= Time.now.strftime('%Y') %> <%= config[:company_name] %>. All rights reserved.
//

#import "<%= "#{name}View.h" %>"
#import <Masonry.h>

@implementation <%= "#{name}View" %>

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self configureViews];
    }
    return self;
}

- (void)configureViews {
    
    [self configureLayouts];
}

- (void)configureLayouts {
    
}

@end
