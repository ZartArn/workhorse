//
//  <%= "#{name}View.h" %>
//  <%= config[:bundle_name] %>
//
//  Created by <%= config[:user_name] %> <%= config[:user_email] %> on <%= Time.now.strftime('%d.%m.%y') %>.
//  Copyright © <%= Time.now.strftime('%Y') %> <%= config[:company_name] %>. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface <%= "#{name}View" %> : UIView

@end
