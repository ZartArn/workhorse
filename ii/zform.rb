class ZFormRow

    attr_reader :pid, :pstyle, :ptitle, :pvalue, :pheight

    def id(id)
        @pid = id
        return self
    end

    def style(style)
        @pstyle = style
        return self
    end

    def title(title)
        @ptitle = title
        return self
    end

    def value(value)
        @pvalue = value
        return self
    end

    def height(height)
        @pheight = height
        return self
    end

end

def ff
    a = [
        ZFormRow.new()
        .id('Myrow')
        .style('RRRR'),

        ZFormRow.new()
            .id('HhhRow')
            .title('YYYYY')
            .style('dfgdfg')
        ]

    return a
end
