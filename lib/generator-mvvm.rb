class GeneratorMVVM < Thor
    include Thor::Actions

    attr_accessor :name, :form_rows, :view_name

    @objc_view_types = {
        'view'          => 'UIView',
        'control'       => 'UIControl',
        'label'         => 'UILabel',
        'text_field'    => 'UITextField',
        'input'         => 'UITextField',
        'text_view'     => 'UITextView',
        'button'        => 'UIButton',
        'switch'        => 'UISwitch',
        'table'         => 'UITableView',
        'collection'    => 'UICollectionView',
        'scroll'        => 'UIScrollView',
        'image'         => 'UIImageView',
        'image_pure'    => 'UIImage',
        'web'           => 'UIWebView',
        'web_view'      => 'UIWebView',
        'progress'      => 'UIProgressView',
        'stack'         => 'UIStackView'
    }

    def self.source_root
        File.dirname(__FILE__)
    end

    def self.create_config
      file_path = File.expand_path('../../config.yml', __FILE__)
      puts file_path
      @config ||= YAML.load_file(file_path)
    end

    # argument :model_name, :attributes
    desc 'create', 'create vmmv module'
    def create (model_name, *attributes)

        # say :model_name
        # say attributes, :red

        name = model_name
        @name = name
        # say args, :green

        opt = GeneratorMVVM.create_config

        # name = 'Ups'; #model_name
        # attributes = args

        @attributes = []
        attributes.each do |attribute|
            field_name, type = attribute.split(':')
            objc_type = GeneratorMVVM.objc_view_type(type)
            @attributes << {
                :type => objc_type,
                :name => field_name
            }       
        end
        say @attributes, :blue

        # name.capitalize!

        template "../template/mvvm/mvvm_view_model.h.erb", "output/#{name}ViewModel.h", opt
        template "../template/mvvm/mvvm_view_model.m.erb", "output/#{name}ViewModel.m", opt

        template "../template/mvvm/mvvm_view_controller.h.erb", "output/#{name}ViewController.h", opt
        template "../template/mvvm/mvvm_view_controller.m.erb", "output/#{name}ViewController.m", opt

        template "../template/mvvm/mvvm_router.h.erb", "output/#{name}Router.h", opt
        template "../template/mvvm/mvvm_router.m.erb", "output/#{name}Router.m", opt

        template "../template/mvvm/mvvm_view.h.erb", "output/#{name}View.h", opt
        template "../template/mvvm/mvvm_view.m.erb", "output/#{name}View.m", opt

        # template "../template/mvvm/photo_view.h.erb", "output/#{name}View.h", opt
        # template "../template/mvvm/photo_view.m.erb", "output/#{name}View.m", opt

        # template "../template/mvvm/photo_view_model.h.erb", "output/#{name}ViewModel.h", opt
        # template "../template/mvvm/photo_view_model.m.erb", "output/#{name}ViewModel.m", opt

        # template "../template/mvvm/photo_controller.h.erb", "output/#{name}ViewController.h", opt
        # template "../template/mvvm/photo_controller.m.erb", "output/#{name}ViewController.m", opt

        # template "../template/mvvm/photo_router.h.erb", "output/#{name}Router.h", opt
        # template "../template/mvvm/photo_router.m.erb", "output/#{name}Router.m", opt

    end

    desc 'form', 'create specific form scene'
    def form(model_name, *attr)
        name = model_name
        @name = name
        @view_name = name        
        opt = GeneratorMVVM.create_config
        @attributes = []

        views = ['bgImageView:image', 'tableView:table']
        views.each do |attribute|
            field_name, type = attribute.split(':')
            objc_type = GeneratorMVVM.objc_view_type(type)
            @attributes << {
                :type => objc_type,
                :name => field_name
            }       
        end
        say @attributes, :blue

        # load form
        # filename = attr.first
        file_path = File.expand_path('../../template/forms.yml', __FILE__)
        forms = YAML.load_file file_path

        step = 1
        forms.each do |form_hash|
            form_hash.each do |key, form|
                @name = key
                opt['form_rows'] = form
                say opt, :green

                if step == 1
                    template "../template/forms/form_view.h.erb", "output/#{@view_name}View.h", opt
                    template "../template/forms/form_view.m.erb", "output/#{@view_name}View.m", opt
                end
                template "../template/forms/form_view_model.h.erb", "output/#{@name}ViewModel.h", opt
                template "../template/forms/form_view_model.m.erb", "output/#{@name}ViewModel.m", opt
                template "../template/forms/form_view_controller.h.erb", "output/#{@name}ViewController.h", opt
                template "../template/forms/form_view_controller.m.erb", "output/#{@name}ViewController.m", opt

                step += 1
                # break
            end
        end
    end

    # argument :model_name, :attributes
    desc 'table', 'create table_view_controller vmmv module'
    def table (model_name, *attributes)

        say model_name, :red
        say attributes, :green
        # say args, :green

        name = model_name
        @name = name

        opt = GeneratorMVVM.create_config

        # name = 'Ups'; #model_name
        # attributes = args

        @attributes = []
        attributes.each do |attribute|
            field_name, type = attribute.split(':')
            objc_type = GeneratorMVVM.objc_view_type(type)
            @attributes << {
                :type => objc_type,
                :name => field_name
            }       
        end
        say @attributes, :blue
        say name, :red

        # name.capitalize!

        template "../template/mvvm/mvvm_view.h.erb", "output/#{name}View.h", opt
        template "../template/mvvm/mvvm_view.m.erb", "output/#{name}View.m", opt

        template "../template/table/mvvm_view_model.h.erb", "output/#{name}ViewModel.h", opt
        template "../template/table/mvvm_view_model.m.erb", "output/#{name}ViewModel.m", opt

        template "../template/mvvm/mvvm_view_controller.h.erb", "output/#{name}ViewController.h", opt
        template "../template/mvvm/mvvm_view_table_controller.m.erb", "output/#{name}ViewController.m", opt

        template "../template/mvvm/mvvm_router.h.erb", "output/#{name}Router.h", opt
        template "../template/mvvm/mvvm_router.m.erb", "output/#{name}Router.m", opt

    end


    no_tasks do
        def self.objc_view_type(type_name)
            @objc_view_types[type_name]
        end
    end
end