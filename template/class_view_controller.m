//
//  <%= "#{name}ViewController.m" %>
//  <%= config[:bundle_name] %>
//
//  Created by <%= config[:user_name] %> <%= config[:user_email] %> on <%= Time.now.strftime('%d.%m.%y') %>.
//  Copyright © <%= Time.now.strftime('%Y') %> <%= config[:company_name] %>. All rights reserved.
//

#import "<%= "#{name}ViewController.h" %>"
#import "<%= "#{name}View.h" %>"
#import "<%= "#{name}ViewModel.h" %>"
#import <ReactiveCocoa.h>

@interface <%= "#{name}ViewController" %> ()

@property (strong, nonatomic) <%= "#{name}ViewModel" %> *viewModel;

@end

@implementation <%= "#{name}ViewController" %>

#pragma mark - UIViewController Life Cycle

- (void)loadView {
    self.view = [<%= "#{name}View" %> new];
    
    <%= "#{name}View" %> *sView = (<%= "#{name}View" %> *)self.view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // enviroment
    
    // assign view model
    self.viewModel = [<%= "#{name}ViewModel" %> new];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


@end

