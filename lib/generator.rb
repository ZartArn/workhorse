# require 'thor'

class Generator < Thor::Group
	include Thor::Actions

	argument :name

	attr_accessor :user

	def self.source_root
		File.dirname(__FILE__)
	end

	def create_lib_file
		# @user = 'ZartArn II'

		opt = {
			:user_name 		=> 'ZartArn',
			:user_email		=> 'lewozart@gmail.com',
			:company_name	=> 'Dem',
			:bundle_name 	=> 'dem.fitnes'
		}

		template "../template/class_view.h", "output/#{name}View.h", opt
		template "../template/class_view.m", "output/#{name}View.m", opt

		template "../template/class_view_model.h", "output/#{name}ViewModel.h", opt
		template "../template/class_view_model.m", "output/#{name}ViewModel.m", opt

		template "../template/class_view_controller.h", "output/#{name}ViewController.h", opt
		template "../template/class_view_controller.m", "output/#{name}ViewController.m", opt
	end
end
