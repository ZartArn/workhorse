//
//  <%= "#{name}ViewModel.h" %>
//  <%= config[:bundle_name] %>
//
//  Created by <%= config[:user_name] %> <%= config[:user_email] %> on <%= Time.now.strftime('%d.%m.%y') %>.
//  Copyright © <%= Time.now.strftime('%Y') %> <%= config[:company_name] %>. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface <%= "#{name}ViewModel" %> : NSObject

@property (strong, nonatomic) NSArray *items;

@end
